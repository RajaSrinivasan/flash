package words_str is 

   SEPARATOR : CHARACTER := ',' ;
   SEPARATOR_STR : String := "," ;

      -------------------------------
     
        -- science -         207 words
        science : String := 
                "ada,aho,algebra,alpha,amneotic,amplify," &
                "anatomy,anode,anole,aortic,arterial,artery," &
                "aspirate,astronomy,atp,atrium,aural,auricle," &
                "axon,azalea,bacteria,beta,bile,biology," &
                "blind,bone,boole,bose,bovine,brain," &
                "calculus,canopy,carotid,cataract,cathode,celiac," &
                "cephalic,cerebral,chandra,chemistry,chiasma,chyme," &
                "cobol,convection,cornea,coronary,cortex,dawkins," &
                "demodex,dendrite,dextrose,dijkstra,dna,duodinum," &
                "eel,einstein,embolus,embryo,enteric,enzyme," &
                "exocrine,eye,faraday,fermat,fetal,fetus," &
                "flow,fluid,flutter,flux,fourier,foxglove," &
                "galaxy,galileo,gamma,ganglion,gastric,gauss," &
                "genesis,gland,glucose,glycan,go,harvey," &
                "heart,heat,hepatic,higgs,hopcroft,hormone," &
                "infect,iris,islet,ivy,javascript,kernighan," &
                "kidney,kidney,knuth,knuth,larynx,laser," &
                "lidar,liver,lung,lymph,manber,marrow," &
                "math,maxwell,microbe,mitosis,mitral,modula," &
                "moray,mrna,mucous,myelin,nerve,neurology," &
                "neuron,newton,nodejs,nucleus,oak,oberon," &
                "occam,optic,ovine,pancreas,panda,paradox," &
                "parotid,pellagra,peptic,peptide,perl,pharynx," &
                "phase,physics,physiology,placenta,placenta,planet," &
                "plasma,platelet,plexus,pollex,porcine,porpose," &
                "pressure,protein,pulsatile,pylorus,python,qualia," &
                "radar,radiation,receptor,renal,retina,ritchie," &
                "rivest,rna,rust,sacks,saline,saliva," &
                "sapience,sapient,scala,schneir,septum,sheath," &
                "signal,sinusoid,sisyphus,skeleton,spasm,spectrum," &
                "spine,spleen,star,stellate,stenosis,stent," &
                "synapse,thermal,thoracic,thrombus,thyroid,tissue," &
                "truncus,turing,ullman,ullman,umbilical,vaquitas," &
                "vectors,vein,venous,vesicle,virus,visceral," &
                "vitamin,volta,wavelet," &
                "parapraxis,cataract,flora,fauna," &
                "" ; 

        -- literature -          67 words
        literature : String := 
                "akilan,alonso,ariel,arundhati,austen,banker," &
                "beckett,boswell,byron,caliban,chanakya,chaucer," &
                "claudius,dickens,doyle,dumas,eliot,faulkner," &
                "gonzolo,hamlet,hardy,harpy,hawthorne,hesse," &
                "hobbit,homer,horatio,jayadeva,johnson,joyce," &
                "kalidasa,kalki,kamban,kannadasan,kipling,macbeth," &
                "melville,miller,miranda,murray,narayan,oneill," &
                "ophelia,orwell,othello,oxfordian,poe,polonius," &
                "prospero,pynchon,rushdie,sandilyan,sartre,sebastian," &
                "steinbeck,stevens,tagore,tempest,tennyson,tolkien," &
                "tolkien,twain,updike,valmiki,vyasa,whitman," &
                "woolf," &
                "" ; 

        -- mythology -          34 words
        mythology : String := 
                "aphrodite,apollo,ares,arjuna,artemis,athene," &
                "atlas,demeter,demeter,dionysus,hades,hanuman," &
                "hathor,hephaestus,hera,hercules,hermes,hestia," &
                "iliad,krishna,lakshmi,nuwa,odyssey,pandava," &
                "pandora,poseidon,prahalada,rama,sisyphus,sita," &
                "thor,troy,vishnu,zeus," &
                "daimon,grimoire,caracteres,micrato,raepy,sathonich,miertr,calamaris," &
                "bharata,lakshmana,ravana,bhishma,duryodana,karnan,sabari,droupathi," &
                "" ; 

        -- words -         175 words
        words : String := 
                "abjure,acrological,acronym,acrostics,allonym,amazeball," &
                "ambigram,ananym,andronym,antagonym,antigram,antilogy," &
                "antonius,antonomasia,antonym,antonym,apricate,aptonym," &
                "arete,autonym,avatar,ax,backronym,basionym," &
                "boob,buoy,busybody,buzz,by,caconym," &
                "capitonym,charactonym,chilliad,chump,clod,conspectus," &
                "contranym,contronym,crayon,cruciverbalist,cryptonym,cuckoo," &
                "darg,demonym,descant,dolt,dupe,enantiodrome," &
                "endonym,endue,eponym,estival,ethnonym,evoo," &
                "ex,exonumia,exonym,fichu,flump,foehn," &
                "foozle,fopdoodle,foyer,fustian,gnashnab,grok," &
                "gull,gynonym,herb,heteronym,hm,hodiernal," &
                "homonym,hubris,hydronym,hyponym,inaptonym,jo," &
                "ka,kennings,keto,klutz,kumbaya,kvell," &
                "lief,liminal,linguonym,lunner,mafflard,maize," &
                "matronym,meed,meronym,metonym,mileway,minatory," &
                "mononym,my,nimrod,noms,numeronym,nundine," &
                "nyctalopia,nycthemeron,oaf,odonym,oronym,ossify," &
                "ox,pangram,paranym,patronym,patronymic,patsy," &
                "perfect,plesionym,proffer,pronym,pseudonym,punct," &
                "qadis,qaids,qajaq,qanat,qapik,qi," &
                "qibla,qophs,qorma,quaff,qubit,quern," &
                "queys,quipu,quire,quoin,quoll,rakefire," &
                "retronym,sachem,sattva,saudade,sedulous,shot-clog," &
                "sinsyne,sizzle,skerrick,smize,snoutband,sorcery," &
                "stampcrab,stooge,strine,synoecious,synoekete,synoicous," &
                "synonychia,synonym,synonym,tautonym,telic,theonym," &
                "tisane,toponym,tousle,trionym,vamoose,veganic," &
                "vexillology,wheeple,xi,xu,za,zooterkins," &
                "zounds," &
                "feriation,chautauqua,notaphily," &
                "assoil," &
                "amphigory," &
                "feriation,haricot," &
                "" ; 

        -- beings -          39 words
        beings : String := 
                "anemone,anthus,arachnid,bobtail,bobwhite,chicadee," &
                "colaptes,corvid,coyote,cuckoo,cygnet,elk," &
                "flicker,flinch,haustellum,jackdaw,limpet,limpet," &
                "loricate,macaw,magpie,mollusk,moose,nematode," &
                "pewee,phoebe,pipit,proboscis,quail,raven," &
                "sequin,spoor,squid,tamarin,tapir,termite," &
                "titmouse,towhee,weddell," &
                "grebe," &
                "alpaca,puffling,puffin," &
                "yunnanozoans," &
                "" ; 

    procedure PrettyPrint( name : string ;
	                       str : string ; 
						   wordcount : integer := 6 ;
						   sorted : boolean := false ) ;
	

end words_str ;
