with Ada.Text_Io; use Ada.Text_Io ;
with gnatcoll.Terminal ; use GNATCOLL.Terminal ;
package body ui is
   Info : GNATCOLL.Terminal.Terminal_Info;
    task body Display is
       mywords : Words_Pkg.Vector ;
       ptr : Integer := 0 ;
       stopped : boolean := false ;
       use Words_Pkg;
    begin
       accept Show( words : Words_Pkg.Vector ) do
            mywords := words ;
        end Show ;

            while not stopped
            loop
                Info.Beginning_Of_Line;
                Info.Clear_To_End_Of_Line;

                for i in 1 .. Integer(mywords.Length) - 1
                loop
                   Put( To_String( mywords.Element(ptr) ) ) ; 
                   Put(" ");
                   ptr := ptr + 1 ;
                   if ptr >= Integer(mywords.Length) 
                   then
                      ptr := 0 ;
                   end if;
                end loop ;
                select
                    accept Stop do
                        stopped := true ;
                        Info.Beginning_Of_Line;
                        Info.Clear_To_End_Of_Line;
                        Put("Stopping...");
                    end Stop;
                or
                    delay 2.0 ;
                end select ;
            end loop ;

    end Display ;


   function Get(Prompt : string) return string is
      result : String(1..80);
      len : natural ;
   begin
      Ada.Text_Io.Put(Prompt) ; Ada.Text_Io.Put(" > "); Ada.Text_Io.Flush ;
      Ada.Text_Io.Get_Line(result,len);
      return result(1..len);
   end Get ;


begin
   Info.Init_For_Stdout (Auto);
   Info.Set_Color (Standard_Output, Red , Yellow);
end ui ;