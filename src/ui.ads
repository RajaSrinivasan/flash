with Ada.Containers.Vectors ;
with Ada.Strings.Unbounded; Use Ada.Strings.Unbounded ;
package ui is
    package Words_Pkg is new Ada.Containers.Vectors( Natural , Unbounded_String );
    task type Display is
        entry Show( words : Words_Pkg.Vector );
        entry Stop ;
    end Display ;

    Play : Display ;

    function Get(prompt : string) return String ;

end ui ;