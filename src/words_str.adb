with Ada.Text_io; use Ada.Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;
with Ada.Strings.Fixed; use Ada.Strings.Fixed ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with Ada.Containers.vectors;
package body words_str is

   subtype WordList_Range is integer range 1..1024 ;
   package WordListPkg is new Ada.Containers.Vectors ( wordlist_range , Ada.Strings.Unbounded.Unbounded_String );
   package wordListSorterPkg is new WordListPkg.Generic_Sorting( Ada.Strings.Unbounded."<") ;

   procedure SortedPrettyPrint( name : string ;
	                             str : string ; 
						              wordcount : integer := 6 ) is
        from, to : Integer ;
        wordlist : WordListPkg.Vector ;
        ptr : wordListPkg.cursor ;
        use wordListPkg ;
        wnum : integer := 0;
    begin
        from := str'first ;
        while from < str'last 
        loop
           for w in 1..wordcount
           loop
              to := Ada.Strings.Fixed.Index(str,SEPARATOR_STR,from) ;
              if to > from
              then
                 wordlist.Append( To_Unbounded_String( str(from..to-1)) );
                 from := to + 1;
              else
                 exit ;
              end if ;
           end loop ;
        end loop ;
        wordListSorterPkg.Sort(wordList) ;
         Put(ASCII.HT); Put("-- "); Put(name); Put(" - "); Put( Ada.Strings.Fixed.Count(str,",")); Put_Line (" words") ;
         Put(ASCII.HT) ; Put(name) ; Put("_pp"); Put(" : String := ") ;  New_Line ;
         ptr := wordList.First ;
         Put(ASCII.HT) ;  Put(ASCII.HT) ;  Put('"') ;

         while ptr /= wordListPkg.No_Element
         loop
            Put(To_String(WordListPkg.Element(ptr)));
            ptr := wordlistPkg.Next(ptr) ;
            wnum := wnum + 1 ;
            if wnum >= wordcount or ptr = WordlistPkg.No_Element
            then
               wnum :=  0 ;
               Put(SEPARATOR_STR); Put('"') ; Put(" &"); New_Line ;
               Put(ASCII.HT) ;  Put(ASCII.HT) ;  Put('"') ;
            else
               Put(SEPARATOR_STR);
            end if ;
         end loop ;
         Put('"') ;
         Put_Line(" ; ") ;
        New_Line ;

    end SortedPrettyPrint ;

    procedure PrettyPrint( name : string ;
	                        str : string ; 
						   wordcount : integer := 6 ;
                     sorted : boolean := false ) is
        from, to : Integer ;
    begin
         if sorted
         then
            SortedPrettyPrint( name , str , wordcount );
            return ;
         end if ;

        Put(ASCII.HT); Put("-- "); Put(name); Put(" - "); Put( Ada.Strings.Fixed.Count(str,",")); Put_Line (" words") ;
        Put(ASCII.HT) ; Put(name) ; Put("_pp"); Put(" : String := ") ;  New_Line ;
        from := str'first ;
        while from < str'last 
        loop
           Put(ASCII.HT) ;  Put(ASCII.HT) ;  Put('"') ;
           for w in 1..wordcount
           loop
              to := Ada.Strings.Fixed.Index(str,SEPARATOR_STR,from) ;
              if to >= from
              then
                 Put(str(from..to)) ;
                 from := to + 1;
              else
                 Put(str(from..str'Last)) ;
                 exit ;
              end if ;
           end loop ;
           Put('"') ; Put("&"); New_Line ;
        end loop ;
        Put(ASCII.HT) ; Put(""""" ; ") ;
        New_Line ;
    end PrettyPrint ;
end words_str ;
