with Ada.Text_Io; use Ada.Text_Io;
with Ada.Short_Short_Integer_Text_Io; use Ada.Short_Short_Integer_Text_Io;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnatcoll.Terminal ; use GNATCOLL.Terminal ;
with words ; 
with numbers ;
with words_str ;

with ui;

procedure Flash is
   Info : GNATCOLL.Terminal.Terminal_Info;
   wl : ui.Words_Pkg.Vector ;
   use ui.Words_Pkg;

   cand : words.CandidateWords_Type ;
   goodrecall : Short_Short_Integer := 0 ;
begin
   cand := words.Initialize (wordlist => words_str.words , separator => words_str.SEPARATOR);

   for seg in 1..2
   loop
      wl.Append(To_Unbounded_String(words.Choose(cand))) ;
      wl.Append(To_Unbounded_String(numbers.Generate)) ;
   end loop ;

   ui.Play.Show(wl) ;
   delay 15.0 ;
   ui.Play.Stop ;
   New_Line ;
   loop
      declare
         wg : constant String := ui.Get("Word") ;
      begin
         if wg = "quit"
         then
            Put("Recalled "); Put(goodrecall); Put_Line(" words.");
            if goodrecall > 1
            then
               Put_Line("Great job.");
            else
               Put_Line("Focus. Focus. Focus.");
            end if ;
            return ;
         end if ;
         if wl.Find( To_Unbounded_String(wg) ) /= ui.Words_Pkg.No_Element
         then
            Put_Line("Good memory....");
            goodrecall := goodrecall + 1 ;
         else
            Put_Line("Sorry. Not one of the words to recall.");
         end if ;
      end ;
   end loop ;

   Info.Set_Color (Standard_Output, Style => Reset_All);

end Flash;
